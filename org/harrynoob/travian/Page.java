package org.harrynoob.travian;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 24-3-13
 * Time: 13:26
 */
public enum Page {
    FIELDS("dorf1"),
    VILLAGE("dorf2"),
    MAP("karte"),
    STATISTICS("statistiken"),
    REPORTS("berichte"),
    MESSAGES("nachrichten");
    private final String suffix;
    Page(final String suffix) {
        this.suffix = suffix;
    }

    public String getSuffix() {
        return suffix;
    }

}
