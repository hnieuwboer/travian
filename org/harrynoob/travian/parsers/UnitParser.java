package org.harrynoob.travian.parsers;

import org.harrynoob.travian.Page;
import org.harrynoob.travian.Parser;
import org.harrynoob.travian.wrappers.Session;
import org.harrynoob.travian.wrappers.Unit;
import org.harrynoob.travian.wrappers.Village;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 24-3-13
 * Time: 13:53
 */
public class UnitParser implements Parser {
    @Override
    public boolean accept(final Page page) {
        return page == Page.FIELDS;
    }

    @Override
    public void parse(final Session session, final Document d) {
        final Elements elements = d.select("td[class=un]");
        final Village village = session.getVillage();
        for(final Element e : elements) {
            final String name = e.text();
            final int amount = Integer.parseInt(e.previousElementSibling().text());
            village.update(new Unit(name), amount);
        }
    }
}
