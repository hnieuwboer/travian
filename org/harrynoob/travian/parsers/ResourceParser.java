package org.harrynoob.travian.parsers;

import org.harrynoob.travian.Page;
import org.harrynoob.travian.Parser;
import org.harrynoob.travian.Resource;
import org.harrynoob.travian.wrappers.Session;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 24-3-13
 * Time: 21:18
 */
public class ResourceParser implements Parser {
    @Override
    public boolean accept(final Page page) {
        return page == Page.FIELDS;
    }

    @Override
    public void parse(final Session session, final Document d) {
        final Elements e = d.select("span[class*=value]");
        for(final Element e1 : e) {
            final String name = e1.previousElementSibling().attr("alt");
            final String[] s = e1.text().split("/");
            for(final Resource r : Resource.values()) {
                if(r.getName().equalsIgnoreCase(name)) {
                    session.updateCurrent(r, Integer.parseInt(s[0]));
                    session.updateCapacity(r, Integer.parseInt(s[1]));
                }
            }
        }
    }
}
