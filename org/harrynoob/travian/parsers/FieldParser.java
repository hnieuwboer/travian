package org.harrynoob.travian.parsers;

import org.harrynoob.travian.Page;
import org.harrynoob.travian.Parser;
import org.harrynoob.travian.wrappers.Building;
import org.harrynoob.travian.wrappers.Session;
import org.harrynoob.travian.wrappers.Village;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 24-3-13
 * Time: 13:38
 */
public class FieldParser implements Parser {
    @Override
    public boolean accept(final Page page) {
        return page == Page.FIELDS;
    }

    @Override
    public void parse(final Session session, final Document d) {
        final Elements areas = d.select("area[alt]");
        final Village village = session.getVillage();
        for(final Element e : areas) {
            if(!e.attr("alt").contains(" ")) {
                continue;
            }
            final String href = e.attr("href");
            final Pattern hrefP = Pattern.compile("build\\.php\\?id=(\\d*)");
            final Matcher hrefM = hrefP.matcher(href);
            final int id = hrefM.find() ? Integer.parseInt(hrefM.group(1)) : -1;
            final String alt = e.attr("alt");
            System.out.println(alt);
            final String name = alt.substring(0, alt.indexOf(' '));
            final int level = Integer.parseInt(alt.charAt(alt.length() - 1) + "");
            final Building building = new Building(id, Building.TYPE_RESOURCE, name);
            building.setLevel(level);
            village.update(building);
        }
        session.updateBuildings();
    }
}
