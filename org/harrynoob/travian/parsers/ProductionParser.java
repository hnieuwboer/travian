package org.harrynoob.travian.parsers;

import org.harrynoob.travian.Page;
import org.harrynoob.travian.Parser;
import org.harrynoob.travian.Resource;
import org.harrynoob.travian.wrappers.Session;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 24-3-13
 * Time: 13:29
 */
public class ProductionParser implements Parser {
    @Override
    public boolean accept(final Page page) {
        return page == Page.FIELDS;
    }

    @Override
    public void parse(final Session session, final Document d) {
        final Elements resources = d.select("td[class=res]");
        System.out.println(resources.size());
        for(final Element e : resources) {
            final Element num = e.nextElementSibling();
            final String name = e.text().substring(0, e.text().indexOf(':'));
            final Resource r = Resource.getResource(name);
            session.updateProduction(r, Integer.parseInt(num.text()));
        }
    }
}
