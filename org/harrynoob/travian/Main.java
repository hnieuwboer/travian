package org.harrynoob.travian;

import org.harrynoob.travian.ui.LoginPanel;
import org.harrynoob.travian.wrappers.Handler;

import javax.swing.*;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 23-3-13
 * Time: 15:05
 */
public class Main {

    private static Handler handler;
    private static LoginPanel gui;
    public static void main(String[] args) {
        handler = new Handler("http://tx3.travian.nl/");
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
        }
        gui = new LoginPanel();
    }

    public static Handler getHandler() {
        return handler;
    }
}
