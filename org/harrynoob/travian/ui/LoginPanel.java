package org.harrynoob.travian.ui;

import org.harrynoob.travian.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 23-3-13
 * Time: 15:41
 */
public class LoginPanel extends JFrame {

    private final JPanel container;
    public LoginPanel() {
        super("Travian GUI");
        final GridBagConstraints c = new GridBagConstraints();
        c.gridy = 0;
        c.gridx = 0;
        c.gridwidth = 5;
        c.anchor = GridBagConstraints.NORTH | GridBagConstraints.CENTER;
        container = new JPanel(new GridBagLayout());
        final JLabel title = new JLabel("TravianGUI");
        title.setFont(new Font("Tahoma", Font.BOLD, 20));
        container.add(title, c);
        c.gridwidth = 2;
        c.gridy = 3;
        c.anchor = GridBagConstraints.CENTER;
        container.add(getLoginPanel(), c);
        this.add(container);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.pack();
        this.setVisible(true);
    }

    private JPanel getLoginPanel() {
        final JPanel panel = new JPanel(new GridBagLayout());
        final GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        panel.add(new JLabel("Username:"), c);
        c.gridx = 1;
        final JTextField username = new JTextField(12);
        panel.add(username, c);
        c.gridx = 0;
        c.gridy = 1;
        panel.add(new JLabel("Password:"), c);
        c.gridx = 1;
        final JPasswordField password = new JPasswordField(12);
        panel.add(password, c);
        c.gridy = 2;
        final JButton b = new JButton("Login");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.getHandler().handleCommand("login " + username.getText() + " " + new String(password.getPassword()));
                final Panel p = new Panel();
                Main.getHandler().setPanel(p);
                setVisible(false);
                p.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                p.setResizable(false);
                p.setVisible(true);
            }
        });
        panel.add(b, c);
        return panel;
    }
}
