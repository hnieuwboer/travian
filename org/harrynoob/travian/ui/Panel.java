/*
 * Created by JFormDesigner on Sun Mar 24 17:29:07 CET 2013
 */

package org.harrynoob.travian.ui;

import org.harrynoob.travian.Main;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

/**
 * @author Harold Nieuwboer
 */
public class Panel extends JFrame {
    public Panel() {
        initComponents();
    }

    private void button1ActionPerformed(ActionEvent e) {
        Main.getHandler().handleCommand("update");
    }

    private void button2ActionPerformed(ActionEvent e) {
        Main.getHandler().handleCommand("upgrade" + String.valueOf(list1.getSelectedIndex() + 1));
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Harold Nieuwboer
        tabbedPane1 = new JTabbedPane();
        panel1 = new JPanel();
        table1 = new JTable();
        panel2 = new JPanel();
        scrollPane1 = new JScrollPane();
        table2 = new JTable();
        panel3 = new JPanel();
        button2 = new JButton();
        vSpacer1 = new JPanel(null);
        vSpacer2 = new JPanel(null);
        scrollPane2 = new JScrollPane();
        list1 = new JList();
        panel4 = new JPanel();
        button1 = new JButton();
        label1 = new JLabel();

        //======== this ========
        setTitle("TravianGUI");
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== tabbedPane1 ========
        {

            //======== panel1 ========
            {

                // JFormDesigner evaluation mark
                panel1.setBorder(new javax.swing.border.CompoundBorder(
                    new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                        "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                        javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                        java.awt.Color.red), panel1.getBorder())); panel1.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

                panel1.setLayout(new BorderLayout());

                //---- table1 ----
                table1.setModel(new DefaultTableModel(
                    new Object[][] {
                        {null, "Current", "Maximum", "Production"},
                        {"Wood", "0", "0", "0"},
                        {"Clay", "0", "0", "0"},
                        {"Iron", "0", "0", "0"},
                        {"Grain", "0", "0", "0"},
                    },
                    new String[] {
                        null, "Current", "Maximum", "Production"
                    }
                ) {
                    Class<?>[] columnTypes = new Class<?>[] {
                        String.class, String.class, String.class, String.class
                    };
                    @Override
                    public Class<?> getColumnClass(int columnIndex) {
                        return columnTypes[columnIndex];
                    }
                });
                {
                    TableColumnModel cm = table1.getColumnModel();
                    cm.getColumn(0).setResizable(false);
                    cm.getColumn(1).setResizable(false);
                    cm.getColumn(2).setResizable(false);
                    cm.getColumn(3).setResizable(false);
                }
                panel1.add(table1, BorderLayout.CENTER);
            }
            tabbedPane1.addTab("Resources", panel1);


            //======== panel2 ========
            {
                panel2.setLayout(new BorderLayout());

                //======== scrollPane1 ========
                {
                    scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

                    //---- table2 ----
                    table2.setModel(new DefaultTableModel(
                        new Object[][] {
                        },
                        new String[] {
                            "Name", "Amount"
                        }
                    ));
                    table2.setPreferredScrollableViewportSize(new Dimension(0, 0));
                    scrollPane1.setViewportView(table2);
                }
                panel2.add(scrollPane1, BorderLayout.CENTER);
            }
            tabbedPane1.addTab("Units", panel2);


            //======== panel3 ========
            {
                panel3.setLayout(new BorderLayout());

                //---- button2 ----
                button2.setText("Upgrade");
                button2.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        button2ActionPerformed(e);
                    }
                });
                panel3.add(button2, BorderLayout.EAST);
                panel3.add(vSpacer1, BorderLayout.SOUTH);
                panel3.add(vSpacer2, BorderLayout.NORTH);

                //======== scrollPane2 ========
                {
                    scrollPane2.setViewportView(list1);
                }
                panel3.add(scrollPane2, BorderLayout.CENTER);
            }
            tabbedPane1.addTab("Buildings", panel3);

        }
        contentPane.add(tabbedPane1, BorderLayout.CENTER);

        //======== panel4 ========
        {
            panel4.setLayout(new BorderLayout());

            //---- button1 ----
            button1.setText("Update");
            button1.setVerticalAlignment(SwingConstants.BOTTOM);
            button1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    button1ActionPerformed(e);
                }
            });
            panel4.add(button1, BorderLayout.SOUTH);

            //---- label1 ----
            label1.setText("Logged in as username");
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setVerticalAlignment(SwingConstants.BOTTOM);
            panel4.add(label1, BorderLayout.NORTH);
        }
        contentPane.add(panel4, BorderLayout.SOUTH);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    public JTable getResourceTable() {
        return table1;
    }

    public JTable getUnitTable() {
        return table2;
    }

    public JList getBuildingList() {
        return list1;
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Harold Nieuwboer
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JTable table1;
    private JPanel panel2;
    private JScrollPane scrollPane1;
    private JTable table2;
    private JPanel panel3;
    private JButton button2;
    private JPanel vSpacer1;
    private JPanel vSpacer2;
    private JScrollPane scrollPane2;
    private JList list1;
    private JPanel panel4;
    private JButton button1;
    private JLabel label1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
