package org.harrynoob.travian;

import org.harrynoob.travian.wrappers.Session;
import org.jsoup.nodes.Document;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 24-3-13
 * Time: 13:24
 */
public interface Parser {
    public boolean accept(final Page page);
    public void parse(final Session session, final Document d);
}
