package org.harrynoob.travian.wrappers;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 24-3-13
 * Time: 12:48
 */
public class Building {

    public static final int TYPE_RESOURCE = 0x1;
    public static final int TYPE_INFRASTRUCTURE = 0x2;
    public static final int TYPE_MILITARY = 0x4;
    public static final int TYPE_RESOURCES = 0x8;

    private final int id;
    private final int type;
    private final String name;
    private int level;
    public Building(final int id, final int type, final String name) {
        this.id = id;
        this.type = type;
        this.name = name;
    }

    public void setLevel(final int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public int getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(final Object o) {
        return o instanceof Building
                && ((Building) o).getId() == id;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Name: ");
        sb.append(name);
        sb.append(" Level: ");
        sb.append(level);
        sb.append(" ID: ");
        sb.append(id);
        return sb.toString();
    }
}
