package org.harrynoob.travian.wrappers;

import org.harrynoob.travian.Page;
import org.harrynoob.travian.Parser;
import org.harrynoob.travian.parsers.FieldParser;
import org.harrynoob.travian.parsers.ProductionParser;
import org.harrynoob.travian.parsers.ResourceParser;
import org.harrynoob.travian.parsers.UnitParser;
import org.harrynoob.travian.ui.Panel;
import org.harrynoob.travian.wrappers.Session;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.JLabel;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 23-3-13
 * Time: 15:37
 */
public class Handler {

    private final String server;
    private final Parser[] parsers = {new FieldParser(), new ProductionParser(), new UnitParser(), new ResourceParser()};
    private Session session;
    private Panel panel;
    public Handler(final String server) {
        this.server = server;
    }

    public void handleCommand(final String command) {
         if(command != null && command.length() > 0) {
            final String[] s = command.split(" ");
            if(s.length > 1) {
                handleCommand(s[0], Arrays.copyOfRange(s, 1, s.length));
            } else {
                handleCommand(s[0], new String[0]);
            }
         }
    }

    private void handleCommand(final String name, final String... args) {
        if (name.equalsIgnoreCase("login") && args.length == 2) {
            login(args[0], args[1]);
        } else if (name.equalsIgnoreCase("update")) {
            update();
        } else if(name.equalsIgnoreCase("upgrade") && args.length == 1) {
            upgrade(Integer.parseInt(args[0]));
        }
    }

    private void login(final String username, final String password) {
        final Connection c = Jsoup.connect(server + "dorf1.php")
                .userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22")
                .data("name", username)
                .data("password", password)
                .data("w", "1280:800")
                .data("s1", "Login")
                .data("login", String.valueOf(System.currentTimeMillis()));
        try {
            c.post();
            final Connection.Response r = c.response();
            session = new Session();
            System.out.println(r.cookies());
            session.setCookie(r.cookies());
            session.setVillage(new Village());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void update() {
        for(final Parser p : parsers) {
            for(final Page p1 : Page.values()) {
                if(p.accept(p1)) {
                    try {
                        final Document d = Jsoup.connect(server + p1.getSuffix() + ".php").cookies(session.getCookie()).get();
                        p.parse(session, d);
                    } catch (IOException iex) {
                        iex.printStackTrace();
                    }
                }
            }
        }
    }

    private boolean upgrade(final int id) {
        try {
            final Document d = Jsoup.connect(server + "build.php?id=" + id).cookies(session.getCookie()).get();
            final Elements e = d.select("div[onclick*=dorf1]");
            if(e.size() == 0) {
                return false;
            }
            final Pattern p = Pattern.compile(".*= 'dorf1\\.php\\?(.*)';.*");
            for(final Element e1 : e) {
                final Matcher m = p.matcher(e1.attr("onclick"));
                if(m.find()) {
                    Jsoup.connect(server + "dorf1.php?" + m.group(1)).cookies(session.getCookie()).get();
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Panel getPanel() {
        return panel;
    }

    public void setPanel(final Panel p) {
        this.panel = p;
    }

}
