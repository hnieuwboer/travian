package org.harrynoob.travian.wrappers;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 24-3-13
 * Time: 13:22
 */
public class Unit {

    private final String name;
    public Unit(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
