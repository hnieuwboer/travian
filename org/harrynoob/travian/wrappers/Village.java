package org.harrynoob.travian.wrappers;

import org.harrynoob.travian.wrappers.Building;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 24-3-13
 * Time: 13:08
 */
public class Village {

    private final List<Building> buildings;
    private final Map<Unit, Integer> units;
    public Village() {
        buildings = new ArrayList<Building>();
        units = new HashMap<Unit, Integer>();
    }

    public void update(final Building building) {
        if(!buildings.contains(building) || buildings.get(buildings.indexOf(building)).getLevel() != building.getLevel()) {
            buildings.remove(building);
            buildings.add(building);
        }
    }

    public Building getBuilding(final int id) {
        for(final Building b : buildings) {
            if(b.getId() == id) {
                return b;
            }
        }
        return null;
    }

    public List<Building> getBuildings() {
        return buildings;
    }

    public void update(final Unit u, final int amount) {
        units.put(u, amount);
    }

    public Map<Unit, Integer> getUnits() {
        return units;
    }

}
