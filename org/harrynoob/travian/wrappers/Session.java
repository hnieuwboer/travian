package org.harrynoob.travian.wrappers;

import org.harrynoob.travian.Main;
import org.harrynoob.travian.Resource;
import org.harrynoob.travian.wrappers.Village;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.*;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 24-3-13
 * Time: 12:47
 */
public class Session {

    private final Map<Resource, Integer> production;
    private final Map<Resource, Integer> capacity;
    private final Map<Resource, Integer> current;
    private final HashMap<Integer, Building> fields;
    private Map<String, String> cookie;
    private Village village;
    public Session() {
        production = new HashMap<Resource, Integer>(4);
        capacity = new HashMap<Resource, Integer>(4);
        current = new HashMap<Resource, Integer>(4);
        fields = new HashMap<Integer, Building>();
        for(final Resource r : Resource.values()) {
            production.put(r, 0);
            capacity.put(r, 0);
            current.put(r, 0);
        }
    }

    public void setCookie(final Map<String, String> cookie) {
        this.cookie = cookie;
    }

    public Map<String, String> getCookie() {
        return cookie;
    }

    public void setVillage(final Village village) {
        this.village = village;
    }

    public Village getVillage() {
        return village;
    }

    public void updateProduction(final Resource r, final int i) {
        production.put(r, i);
        updateTable(i, r.getIndex(), 3);
    }

    public Map<Resource, Integer> getProduction() {
        return production;
    }

    public int getProduction(final Resource r) {
        return production.get(r);
    }

    public void updateCurrent(final Resource r, final int i) {
        current.put(r, i);
        updateTable(i, r.getIndex(), 1);
    }

    public void updateCapacity(final Resource r, final int i) {
        capacity.put(r, i);
        updateTable(i, r.getIndex(), 2);
    }

    private void updateTable(final int data, final int idx, final int col) {
        final JTable jt = Main.getHandler().getPanel().getResourceTable();
        final DefaultTableModel dtm = (DefaultTableModel) jt.getModel();
        dtm.setValueAt(String.valueOf(data), idx, col);
    }

    public void updateBuildings() {
        final JList list = Main.getHandler().getPanel().getBuildingList();
        final DefaultListModel model = new DefaultListModel();
        for(final Building building : village.getBuildings()) {
            model.addElement(building);
        }
        list.setModel(model);
    }
}
