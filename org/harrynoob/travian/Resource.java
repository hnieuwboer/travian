package org.harrynoob.travian;

/**
 * Do not use this without permission.
 * User: harrynoob
 * Date: 23-3-13
 * Time: 16:26
 */
public enum Resource {
    WOOD(1, "Hout", "Houthakker"),
    CLAY(2, "Klei", "Klei-afgraving"),
    IRON(3, "IJzer", "Ijzermijn"),
    GRAIN(4, "Graan", "Graanakker");
    private final String name;
    private final int idx;
    private final String fieldName;
    Resource(final int idx, final String name, final String fieldName) {
        this.idx = idx;
        this.name = name;
        this.fieldName = fieldName;
    }

    public int getIndex() {
        return idx;
    }

    public String getName() {
        return name;
    }

    public String getFieldName() {
        return fieldName;
    }

    public static Resource getResource(final String name) {
        for(final Resource r : Resource.values()) {
            if(r.getName().equals(name)) {
                return r;
            }
        }
        return null;
    }
}
